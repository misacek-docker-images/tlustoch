.DEFAULT_GOAL := help

include mk.d/Makefile.portability
include mk.d/Makefile.labels

export DOCKER_IMAGE_REPOSITORY ?= docker-registry.engineering.redhat.com
export DOCKER_IMAGE_PREFIX ?= mnovacek
export DOCKER_IMAGE_TAG ?= :$(LABEL_VERSION)

DISTROS := centos7 centos7-systemd centos6 debian
STOP := $(addprefix stop-,$(DISTROS))
BUILD := $(addprefix build-,$(DISTROS))
PUSH := $(addprefix push-,$(DISTROS))
PULL := $(addprefix pull-,$(DISTROS))
RUN := $(addprefix run-,$(DISTROS))

help:
	@echo Targets: [run,stop,push,build,all]-[debian,centos7,centos6]

$(PUSH):
	docker-compose push $(@:push-%=%)

$(PULL):
	docker-compose pull $(@:pull-%=%)

$(BUILD):
	docker-compose build $(@:build-%=%)

$(RUN):
	docker-compose up -d $(@:run-%=%)

$(STOP):
	docker-compose stop $(@:stop-%=%)

push: $(PUSH)
	@echo "$$(date): $@: success"

build: $(BUILD)
	@echo "$$(date): $@: success"

all: | build push
	@echo "$$(date): $@: success"

stop: $(STOP)
	@echo "$$(date): $@: success"

run: $(RUN)
	@echo "centos7: ssh -p 55022 root@localhost"
	@echo "centos6: ssh -p 55023 root@localhost"
	@echo "debian9: ssh -p 55024 root@localhost"

show-config:
	echo $(DOCKER_IMAGE_TAG)
	docker-compose config

all: | build push
	@echo "$$(date): $@: success"

stop:
	docker-compose stop

run: $(RUN)
	@echo "centos7: ssh -p 55022 root@localhost"
	@echo "centos6: ssh -p 55023 root@localhost"
	@echo "debian9: ssh -p 55024 root@localhost"
